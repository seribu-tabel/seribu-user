package id.seribu.seribuuser.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import id.seribu.seribudto.shared.Lov;
import id.seribu.seribudto.template.ApiResponse;
import id.seribu.seribudto.user.UserRequestDto;
import id.seribu.seribudto.user.UserResponseDto;
import id.seribu.seribuuser.service.UserService;
import lombok.extern.slf4j.Slf4j;

@RestController
@Slf4j
public class UserController{

    @Autowired
    private UserService userService;

    @PreAuthorize("hasRole('ROLE_SUPER') or hasRole('ROLE_ADMIN')")
    @PostMapping(path = "${application.endpoints.user-service.signup}", 
        consumes = MediaType.APPLICATION_JSON_VALUE,
        produces = MediaType.APPLICATION_JSON_VALUE)
    public ApiResponse<UserResponseDto> signUp(@RequestBody UserRequestDto userRequestDto){
        log.info("UserController.signUp execute, username : {}", userRequestDto.getUsername());
        UserResponseDto userResponseDto = userService.save(userRequestDto);
        return new ApiResponse<>(HttpStatus.OK.value(), "Data user berhasil disimpan", userResponseDto);
    }

    @PreAuthorize("hasRole('ROLE_SUPER') or hasRole('ROLE_ADMIN')")
    @GetMapping(path = "${application.endpoints.user-service.roles}", produces = MediaType.APPLICATION_JSON_VALUE)
    public ApiResponse<Object> findAllEnum(){
        List<Lov> allRoles = userService.findAllRole();
        return new ApiResponse<>(HttpStatus.OK.value(), "List roles", allRoles);
    }

    @PreAuthorize("hasRole('ROLE_SUPER')")
    @GetMapping(path = "${application.endpoints.user-service.active-user}", produces = MediaType.APPLICATION_JSON_VALUE)
    public ApiResponse<List<UserResponseDto>> findAllActiveUser(){
        List<UserResponseDto> allUsers = userService.findAllActiveUser();
        return new ApiResponse<>(HttpStatus.OK.value(), "List users", allUsers);
    }

    @PreAuthorize("hasRole('ROLE_ADMIN')")
    @GetMapping(path = "${application.endpoints.user-service.active-user-only}", produces = MediaType.APPLICATION_JSON_VALUE)
    public ApiResponse<List<UserResponseDto>> findAllActiveUserOnly(){
        List<UserResponseDto> allUsers = userService.findAllActiveUserOnly();
        return new ApiResponse<>(HttpStatus.OK.value(), "List users with user role only", allUsers);
    }

    @PreAuthorize("hasRole('ROLE_SUPER') or hasRole('ROLE_ADMIN')")
    @PostMapping(path = "${application.endpoints.user-service.update}", 
        consumes = MediaType.APPLICATION_JSON_VALUE,
        produces = MediaType.APPLICATION_JSON_VALUE)
    public ApiResponse<UserResponseDto> update(@RequestBody UserRequestDto userRequestDto) {
        log.info("UserController.update execute, userRequest : {}", userRequestDto.toString());
        UserResponseDto userResponseDto = userService.update(userRequestDto, userRequestDto.isEnabled());
        return new ApiResponse<>(HttpStatus.OK.value(), "Data user berhasil diupdate", userResponseDto);
    }

    @PreAuthorize("hasRole('ROLE_SUPER') or hasRole('ROLE_ADMIN')")
    @GetMapping(path = "${application.endpoints.user-service.find-by-username}")
    public ApiResponse<UserResponseDto> findByUsername(@PathVariable("username") String username) {
        log.info("UserController.findByUsername execute, username : {}", username);
        UserResponseDto userResponseDto = userService.findByUsername(username);
        return new ApiResponse<>(HttpStatus.OK.value(), "Data ditemukan", userResponseDto);
    }
}