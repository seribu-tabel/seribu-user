package id.seribu.seribuuser.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import id.seribu.seribudto.template.ApiResponse;
import id.seribu.seribudto.user.ActivityDto;
import id.seribu.seribuuser.service.ActivityLogService;
import lombok.extern.slf4j.Slf4j;

@Slf4j
@RestController
public class ActivityController {

    @Autowired
    ActivityLogService activityLogService;

    @PostMapping(path = "${application.endpoints.user-service.save-log}"
        , consumes = MediaType.APPLICATION_JSON_VALUE)
    public ApiResponse<String> saveActiviyLog(@RequestBody ActivityDto activityDto) {
        log.info("ActivityController.saveActiviyLog data: {}", activityDto.toString());
        activityLogService.save(activityDto);
        return new ApiResponse<String>(HttpStatus.OK.value(), "log berhasil disimpan", "LOGGED");
    }

    @PreAuthorize("hasRole('ROLE_SUPER') or hasRole('ROLE_ADMIN')")
    @GetMapping(path = "${application.endpoints.user-service.all-log}")
    public ApiResponse<List<ActivityDto>> getAllActivityLog() {
        log.info("ActivityController.getAllActivityLog");
        return new ApiResponse<List<ActivityDto>>(HttpStatus.OK.value(), "list all log", activityLogService.findAllLog());
    }

    @GetMapping(path = "${application.endpoints.user-service.search-log}")
    public ApiResponse<List<ActivityDto>> searchActivity(@RequestParam(value = "search") String search) {
        log.info("ActivityController.searchActivity");
        return new ApiResponse<List<ActivityDto>>(HttpStatus.OK.value(), "search activity", activityLogService.searchActivity(search));
    }
}