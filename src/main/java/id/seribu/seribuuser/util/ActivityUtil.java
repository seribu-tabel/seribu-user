package id.seribu.seribuuser.util;

import java.util.Date;
import java.util.UUID;

import org.springframework.stereotype.Component;

import id.seribu.seribudto.user.ActivityDto;
import id.seribu.seribumodel.user.MActivity;

@Component
public class ActivityUtil {

    public MActivity createModelActivity(ActivityDto dto) {
        MActivity activity = new MActivity();
        activity.setId(UUID.randomUUID());
        activity.setUsername(dto.getUsername());
        activity.setNip(dto.getNip());
        activity.setAuthority(dto.getAuthority());
        activity.setIpAddress(dto.getIpAddress());
        activity.setActivity(dto.getActivity());
        activity.setDataset(dto.getDataset());
        activity.setSubyekData(dto.getSubyekData());
        activity.setSourceData(dto.getSourceData());
        activity.setFirstCharacter(dto.getFirstCharacter());
        activity.setSecondCharacter(dto.getSecondCharacter());
        activity.setAccessDate(dto.getAccessDate());
        activity.setCreatedDate(new Date());
        return activity;
    }
}