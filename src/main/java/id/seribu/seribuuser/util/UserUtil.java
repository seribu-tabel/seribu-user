package id.seribu.seribuuser.util;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.UUID;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Component;

import id.seribu.seribudto.user.UserRequestDto;
import id.seribu.seribudto.user.UserResponseDto;
import id.seribu.seribumodel.enumeration.RoleEnum;
import id.seribu.seribumodel.user.MUser;
import lombok.extern.slf4j.Slf4j;

@Component
@Slf4j
public class UserUtil {

    @Autowired
    private BCryptPasswordEncoder bCryptPasswordEncoder;

    public MUser createUserModel(UserRequestDto userDto) {
        log.info("UserUtil.createUserModel execute: {}", userDto.toString());
        MUser user = new MUser();
        try {
            user.setId(UUID.randomUUID());
            user.setNip(userDto.getNip());
            user.setUsername(userDto.getUsername());
            user.setName(userDto.getName());
            user.setPassword(bCryptPasswordEncoder.encode(userDto.getPassword()));
            user.setRoles(RoleEnum.valueOf(userDto.getRole()));
            user.setEnabled(true);
            user.setCreatedDate(new Date());
        } catch (Exception e) {
            log.error("fail UserUtil.createUserModel, username {}", user.toString(), e);
        }
        return user;
    }

    public MUser editOrDel(UUID id, UserRequestDto userDto, boolean isEnabled) {
        log.info("UserUtil.editOrDel execute: ID {}, {}", id, userDto.toString());
        MUser user = new MUser();
        try {
            user.setId(id);
            user.setNip(userDto.getNip());
            user.setUsername(userDto.getUsername());
            user.setName(userDto.getName());
            user.setPassword(bCryptPasswordEncoder.encode(userDto.getPassword()));
            user.setRoles(RoleEnum.valueOf(userDto.getRole().toUpperCase()));
            if (isEnabled) {
                user.setEnabled(true);
            } else {
                user.setEnabled(false);
            }
            user.setCreatedDate(userDto.getCreatedDate());
            user.setUpdatedDate(new Date());
        } catch (Exception e) {
            log.error("fail UserUtil.editOrDel, username {}", userDto.getUsername(), e);
        }
        return user;
    }

    public UserResponseDto generateUserResponseDto(MUser mUser) {
        log.info("UserUtil.generateUserResponseDto execute: {}", mUser.toString());
        UserResponseDto userDto = new UserResponseDto();
        userDto.setNip(mUser.getNip());
        userDto.setName(mUser.getName());
        userDto.setUsername(mUser.getUsername());
        userDto.setRole(mUser.getRoles().toString());
        userDto.setPassword(mUser.getPassword());
        return userDto;
    }

    public List<UserResponseDto> generateListUserResponse(List<MUser> mUser){
        log.info("UserUtil.generateListUserResponse execute: list size {}", mUser.size());
        List<UserResponseDto> listUser = new ArrayList<>(); 
        listUser = mUser.stream()
            .map(u -> new UserResponseDto(u.getId(), u.getNip(), u.getName(), 
                u.getUsername(), u.getRoles().toString(), u.getRoles().getText(), 
                u.getPassword(), u.isEnabled(), u.getCreatedDate()))
            .collect(Collectors.toList());
        return listUser;
    }
}