package id.seribu.seribuuser.util;

import java.util.ArrayList;
import java.util.List;

import id.seribu.seribudto.shared.SearchCriteria;
import id.seribu.seribuuser.repository.ActivitySpecification;

public class ActivityLogSpecificationBuilder {
    
    private List<SearchCriteria> params;

    public ActivityLogSpecificationBuilder() {
        this.params = new ArrayList<>();
    }

    public ActivityLogSpecificationBuilder with(String key, String operation, Object value) {
        params.add(new SearchCriteria(key, operation, value, true));
        return this;
    }
 
    public ActivitySpecification build() {
        if (params.size() == 0) {
            return null;
        }
         
        ActivitySpecification actSpecs = new ActivitySpecification();   
        actSpecs.addAll(params);
        return actSpecs;
    }
}