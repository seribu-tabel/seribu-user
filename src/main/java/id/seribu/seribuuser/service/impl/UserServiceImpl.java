package id.seribu.seribuuser.service.impl;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import id.seribu.seribudto.shared.Lov;
import id.seribu.seribudto.user.UserRequestDto;
import id.seribu.seribudto.user.UserResponseDto;
import id.seribu.seribumodel.enumeration.RoleEnum;
import id.seribu.seribumodel.user.MUser;
import id.seribu.seribuuser.repository.UserRepository;
import id.seribu.seribuuser.service.UserService;
import id.seribu.seribuuser.util.UserUtil;
import lombok.extern.slf4j.Slf4j;

/**
 * @author arieki
 */
@Service
@Slf4j
public class UserServiceImpl implements UserService {

    @Autowired
    private UserRepository userRepository;

    @Autowired
    private UserUtil userUtil;

    @Override
    @Transactional
    public UserResponseDto save(UserRequestDto userRequestDto) {
        log.info("UserService.save, username : {}", userRequestDto.getUsername());
        List<MUser> findUserByNip = userRepository.findByNip(userRequestDto.getNip());
        if(findUserByNip != null || findUserByNip.size() > 0) {
            userRepository.deleteByNip(userRequestDto.getNip());
        }
        MUser mUser = new MUser();
        mUser = userUtil.createUserModel(userRequestDto);
        mUser = userRepository.save(mUser);
        return userUtil.generateUserResponseDto(mUser);
    }

    @Override
    @Transactional
    public UserResponseDto update(UserRequestDto userRequestDto, boolean isEnabled) {
        log.info("UserService.update, username : {}, nip: {}", userRequestDto.getUsername(), userRequestDto.getNip());
        List<MUser> mUser = userRepository.findByNip(userRequestDto.getNip());
        MUser editedUser = new MUser();
        if (mUser != null || mUser.size() > 0) {
            editedUser = userUtil.editOrDel(mUser.get(0).getId(), userRequestDto, isEnabled);
            if (isEnabled) {
                userRepository.save(editedUser);
            } else {
                userRepository.deleteByNip(userRequestDto.getNip());
            }
        }
        return userUtil.generateUserResponseDto(editedUser);
    }

    @Override
    public UserResponseDto findByUsername(String username) {
        log.info("UserService.findByUsername, username :  {}", username);
        MUser mUser = userRepository.findByUsername(username);
        return userUtil.generateUserResponseDto(mUser);
    }

    @Override
    public List<UserResponseDto> findAllActiveUser() {
        log.info("UserService.findAllActiveUser");
        List<UserResponseDto> listUserResponse = new ArrayList<>();
        List<MUser> listUser = userRepository.findAllActiveUserAllRoles();
        listUserResponse = userUtil.generateListUserResponse(listUser);
        return listUserResponse;
    }

    @Override
    public List<UserResponseDto> findAllActiveUserOnly() {
        log.info("UserService.findAllActiveUserOnly");
        List<UserResponseDto> listUserResponse = new ArrayList<>();
        List<MUser> listUser = userRepository.findAllActiveUserOnly();
        listUserResponse = userUtil.generateListUserResponse(listUser);
        return listUserResponse;
    }

    @Override
    public List<Lov> findAllRole() {
        log.info("UserService.findAllRole");
        List<Lov> enumLov = new ArrayList<>();
        RoleEnum[] arrayEnum = RoleEnum.values();
        for (RoleEnum num : arrayEnum) {
            Lov lov = new Lov();
            lov.setValue(num.toString());
            lov.setText(num.getText());
            enumLov.add(lov);
        }
        return enumLov;
    }

}