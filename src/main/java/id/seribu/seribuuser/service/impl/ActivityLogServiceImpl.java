package id.seribu.seribuuser.service.impl;

import java.util.ArrayList;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import id.seribu.seribudto.user.ActivityDto;
import id.seribu.seribumodel.user.MActivity;
import id.seribu.seribuuser.repository.ActivityLogRepository;
import id.seribu.seribuuser.repository.ActivitySpecification;
import id.seribu.seribuuser.service.ActivityLogService;
import id.seribu.seribuuser.util.ActivityLogSpecificationBuilder;
import id.seribu.seribuuser.util.ActivityUtil;
import lombok.extern.slf4j.Slf4j;

@Service
@Slf4j
public class ActivityLogServiceImpl implements ActivityLogService {

    @Autowired
    ActivityUtil activityUtil;

    @Autowired
    ActivityLogRepository activityLogRepository;

    @Override
    public void save(ActivityDto activityDto) {
        log.info("ActivityLogService.save data: {}", activityDto.toString());
        activityLogRepository.save(activityUtil.createModelActivity(activityDto));
    }

    @Override
    public List<ActivityDto> findAllLog() {
        log.info("ActivityLogService.findAllLog");
        List<MActivity> lActivity = new ArrayList<>();
        activityLogRepository.findAll().iterator().forEachRemaining(lActivity::add);
        return lActivity.stream().map(m -> {
            ActivityDto dtos = new ActivityDto();
            dtos.setUsername(m.getUsername());
            dtos.setNip(m.getNip());
            dtos.setAuthority(m.getAuthority());
            dtos.setIpAddress(m.getIpAddress());
            dtos.setActivity(m.getActivity());
            dtos.setDataset(m.getDataset());
            dtos.setSubyekData(m.getSubyekData());
            dtos.setSourceData(m.getSourceData());
            dtos.setFirstCharacter(m.getFirstCharacter());
            dtos.setSecondCharacter(m.getSecondCharacter());
            dtos.setAccessDate(m.getAccessDate());
            return dtos;
        }).collect(Collectors.toList());
    }

    @Override
    public List<ActivityDto> searchActivity(String searchCriteria) {
        log.info("ActivityLogService.searchActivity [criteria: {}]", searchCriteria);
        List<MActivity> lActivity = new ArrayList<>();
        ActivityLogSpecificationBuilder builder = new ActivityLogSpecificationBuilder();
        if(searchCriteria != null) {
            Pattern pattern = Pattern.compile("(\\w+?)(:)(\\w+?);");
            Matcher matcher = pattern.matcher(searchCriteria + ";");
            while(matcher.find()) {
                builder.with(matcher.group(1), matcher.group(2), matcher.group(3));
            }
        }
        ActivitySpecification spec = builder.build();
        activityLogRepository.findAll(spec).iterator().forEachRemaining(lActivity::add);
        return lActivity.stream().map(m -> {
            ActivityDto dtos = new ActivityDto();
            dtos.setUsername(m.getUsername());
            dtos.setNip(m.getNip());
            dtos.setAuthority(m.getAuthority());
            dtos.setIpAddress(m.getIpAddress());
            dtos.setActivity(m.getActivity());
            dtos.setDataset(m.getDataset());
            dtos.setSubyekData(m.getSubyekData());
            dtos.setSourceData(m.getSourceData());
            dtos.setFirstCharacter(m.getFirstCharacter());
            dtos.setSecondCharacter(m.getSecondCharacter());
            dtos.setAccessDate(m.getAccessDate());
            return dtos;
        }).collect(Collectors.toList());
    }
}