package id.seribu.seribuuser.service;

import java.util.List;

import id.seribu.seribudto.shared.Lov;
import id.seribu.seribudto.user.UserRequestDto;
import id.seribu.seribudto.user.UserResponseDto;


/**
 * @author arieki 
*/
public interface UserService{

    UserResponseDto save(UserRequestDto userRequestDto);
    UserResponseDto update(UserRequestDto userRequestDto, boolean isEnabled);
    UserResponseDto findByUsername(String username);
    List<UserResponseDto> findAllActiveUser();
    List<UserResponseDto> findAllActiveUserOnly();

    List<Lov> findAllRole();
}