package id.seribu.seribuuser.service;

import java.util.List;

import id.seribu.seribudto.user.ActivityDto;

public interface ActivityLogService {

    void save(ActivityDto activityDto);

    List<ActivityDto> findAllLog();

    List<ActivityDto> searchActivity(String searchCriteria);
}