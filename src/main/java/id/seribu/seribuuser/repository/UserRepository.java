package id.seribu.seribuuser.repository;

import java.util.List;
import java.util.UUID;

import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import id.seribu.seribumodel.user.MUser;

/**
 * @author arieki
 */
@Repository
public interface UserRepository extends CrudRepository<MUser, UUID>{

    @Query(value = "select *from m_user where enabled = 'true' and username = :username", nativeQuery = true)
    MUser findByUsername(@Param("username") String username);

    @Query(value = "select *from m_user where enabled = 'true'and nip = :nip", nativeQuery = true)
    List<MUser> findByNip(@Param("nip") String nip);

    @Query(value = "SELECT * FROM m_user"
            + " where enabled = 'true'"
            + " and role != 'ROLE_SUPER'"
        , nativeQuery = true)
    List<MUser> findAllActiveUserAllRoles();

    @Query(value = "SELECT * FROM m_user"
            + " where enabled = 'true'"
            + " and role = 'ROLE_USER'"
        , nativeQuery = true)
    List<MUser> findAllActiveUserOnly();

    @Modifying
    @Query(value = "delete from m_user where nip = :nip", nativeQuery = true)
    void deleteByNip(@Param("nip") String nip);

}