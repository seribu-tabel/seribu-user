package id.seribu.seribuuser.repository;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;

import org.springframework.data.jpa.domain.Specification;

import id.seribu.seribudto.shared.SearchCriteria;
import id.seribu.seribumodel.user.MActivity;
import lombok.Data;

@Data
public class ActivitySpecification implements Specification<MActivity> {

    /**
     *
     */
    private static final long serialVersionUID = 1L;

    private List<SearchCriteria> listCriteria;

    public ActivitySpecification() {
        this.listCriteria = new ArrayList<>();
    }

    public void add(SearchCriteria criteria) {
        listCriteria.add(criteria);
    }

    public void addAll(List<SearchCriteria> listCriteria) {
        this.listCriteria.addAll(listCriteria);
    }

    @Override
    public Predicate toPredicate(Root<MActivity> root, CriteriaQuery<?> query, CriteriaBuilder criteriaBuilder) {
        List<Predicate> predicates = new ArrayList<>();
        for (SearchCriteria criteria : listCriteria) {
            if (criteria.getOperation().equalsIgnoreCase(":")) {
                predicates.add(criteriaBuilder.like(criteriaBuilder.lower(root.get(criteria.getKey())),
                        "%" + criteria.getValue().toString().toLowerCase() + "%"));
            }

        }
        return criteriaBuilder.or(predicates.toArray(new Predicate[0]));
    }

}