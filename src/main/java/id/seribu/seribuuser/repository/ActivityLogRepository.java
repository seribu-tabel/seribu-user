package id.seribu.seribuuser.repository;

import java.util.UUID;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.stereotype.Repository;

import id.seribu.seribumodel.user.MActivity;

@Repository
public interface ActivityLogRepository extends JpaRepository<MActivity, UUID>, JpaSpecificationExecutor<MActivity> {

}