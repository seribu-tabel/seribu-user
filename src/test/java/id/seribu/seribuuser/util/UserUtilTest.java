package id.seribu.seribuuser.util;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.test.context.junit4.SpringRunner;

import id.seribu.seribudto.user.UserRequestDto;
import id.seribu.seribumodel.enumeration.RoleEnum;
import id.seribu.seribumodel.user.MUser;


@RunWith(SpringRunner.class)
@SpringBootTest(classes = {UserUtil.class, BCryptPasswordEncoder.class})
public class UserUtilTest {

    // @InjectMocks
    @Autowired
    UserUtil userUtil;

    UserRequestDto requestDto;
    MUser userExpectation;

    @Before
    public void setup() {
        requestDto = new UserRequestDto();
        requestDto.setNip("100000000");
        requestDto.setName("dummy 3");
        requestDto.setUsername("dum3");
        requestDto.setPassword("password");
        requestDto.setRole("ROLE_USER");

        userExpectation = new MUser();
        userExpectation.setName("dummy 3");
        userExpectation.setNip("100000000");
        userExpectation.setUsername("dum3");
        userExpectation.setRoles(RoleEnum.ROLE_USER);
    }

    @Test
    public void createUserModelTest() {
        MUser userActual = userUtil.createUserModel(requestDto);
        Assert.assertEquals(userExpectation.getName(), userActual.getName());
        Assert.assertEquals(userExpectation.getNip(), userActual.getNip());
        Assert.assertEquals(userExpectation.getUsername(), userActual.getUsername());
        Assert.assertEquals(userExpectation.getRoles(), userActual.getRoles());
    }
}